#!/usr/bin/python

# Integrates the equations of motion of the stochastically-forced Lorenz attractor via a fourth-order Runge-Kutta time step. Then constructs and plots its power spectrum by performing a Fourier transform on the time series of its x-values. Written by Altan Allawala.

import math
from numpy import *
from pylab import *
from scipy import weave
import scipy.interpolate
import scipy.ndimage.interpolation
from scipy.weave import converters
from scipy.integrate import odeint
from matplotlib.colors import LogNorm
from scipy.interpolate import interp1d
from mpl_toolkits.axes_grid1 import make_axes_locatable

import time

class Lorenz (object): 
	  
	def __init__(self, x=0.0, y=0.0, z=0.0, gamma = 10., endTime = 100.0, dt = 0.01, N = 100):
		self.dt = dt
		self.gamma = gamma
		self.endTime = endTime
		self.phaseSpace = array([x, y, z])
		self.phaseSpacestore = np.zeros((int(self.endTime/self.dt),3))
		self.N = N
		self.Nt = int(self.endTime/self.dt)


	def CreateStochasticForcing(self): # Require that dt<<deltaT<<T
		print "Creating random gaussian array"

		T = 1. #T = 0.1 # Time-scale of the system (this is set by the attractor itself)
		deltaT = sqrt(T*self.dt) # Time-scale over which to vary the stochastic forcing
		Nt = self.Nt # Number of time steps
		coursex = arange(0.,self.endTime+deltaT,deltaT) # Create stochastic points seperated by deltaT
		nt = len(coursex)
		coursef = sqrt(2.0 * self.gamma/deltaT) * np.random.normal(loc=0., scale=1., size = (nt,3))

		print "Interpolating"
		finex = arange(0.,self.endTime,self.dt)
		finefx =  np.interp(finex, coursex, coursef[:,0])
		finefy =  np.interp(finex, coursex, coursef[:,1])
		finefz =  np.interp(finex, coursex, coursef[:,2])

		self.f = concatenate((finefx,finefy,finefz), axis=0)
		self.f = reshape(self.f, (Nt,3), order='F')


	def RHS(self, xarray, f):

		x = xarray[0]
		y = xarray[1]
		z = xarray[2]

		sigma = 3.#10.
		rho = 26.5#28.
		beta = 0.16#1.#8./3.

		xdot = sigma * (y - x) + f[0]
		ydot = x * (rho - z) - y + f[1]
		zdot = x * y - beta * z + f[2]
	
		return array([xdot, ydot, zdot])


	def integrate(self): # Fourth Order Runge-Kutta method
		print "Integrating"
		t = 0.
		i = 0
		Nt = self.Nt

		f = self.f

		for i in range(Nt):
			self.k1 = self.dt * self.RHS(self.phaseSpace, f[i,:])
			self.k2 = self.dt * self.RHS(self.phaseSpace+0.5*self.k1, f[i,:])
			self.k3 = self.dt * self.RHS(self.phaseSpace+0.5*self.k2, f[i,:])
			self.k4 = self.dt * self.RHS(self.phaseSpace+self.k3, f[i,:])
			self.phaseSpace = self.phaseSpace + self.k1/6. + self.k2/3. + self.k3/3. + self.k4/6.
			self.phaseSpacestore[i]=self.phaseSpace
			i = i + 1
			if (i%100000==0):
				print "Time = ", i*self.dt

		self.TimeSeries = self.phaseSpacestore[:,0] # SELECT x VALUES
                

	def FFT(self): # Fast Fourier transform
		import numpy.fft
		ft = rfft(self.TimeSeries) # Take the FT
		self.PowerSpectrum = np.abs(ft)**2


	def Binning(self): # Bin the data points into a histogram

		print "Taking FT, ", time.clock()
		freqs = rfftfreq(len(self.PowerSpectrum)*2-2 , d=self.dt)

		print "Binning data, ", time.clock()
		bins = int(freqs[-1]/self.dt)+1
		from scipy.stats import binned_statistic
		self.PowerSpectrum = binned_statistic(freqs, self.PowerSpectrum, bins=bins)[0]
		self.freqs = np.linspace(0, freqs[-1], num=bins)

		print "Combining data, ", time.clock()
		self.Data = array([self.freqs,self.PowerSpectrum])


	def SaveData(self):
#		print "Saving time series"
#		np.savetxt('TimeSeries', self.TimeSeries)

		print "Saving binned power spectrum, ", time.clock()
		np.savetxt('PowerSpectrum', self.Data)


	def LoadData(self): # Load data of the same form as is saved
		print "Loading data, ", time.clock()
		self.Data = np.loadtxt('PowerSpectrum')
#		self.TimeSeries = np.loadtxt('TimeSeries')


	def Plot(self): # Plot the power spectrum of the Lorenz attractor

##		print "Plotting time series"
#		plot(self.TimeSeries)#, color='black',linestyle='--', linewidth=2.)#, vmin=0., vmax=4., c=c1, cmap=cmap)

		print "Plotting frequency spectrum, ", time.clock()

		figure() # Plot the Power Spectrum
		plot(self.Data[0,:], self.Data[1,:], 'b-')
		xlim(0,1.5)
		xlabel('Frequency')
		ylabel('Power Spectrum')
		savefig('PowerSpectrum.pdf')



		
p = Lorenz(x=1., y=1., z=25., gamma=0.01, endTime=1000., dt=.01, N=478)

p.CreateStochasticForcing()
p.integrate()
p.FFT()
p.Binning()
p.SaveData()

p.LoadData()
p.Plot()
show()
