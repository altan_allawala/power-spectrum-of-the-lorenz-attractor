# README #

This code was written for a research project on the statistics of the stochastically-forced Lorenz attractor.

It has two steps:

1) Accumulates the statistics of the PDF of the attractor by integrating its equations of motion via a fourth-order Runge-Kutta time step.

2) Constructs and plots its power spectrum by performing a Fourier transform on the time series of its x-values.

Written by Altan Allawala.